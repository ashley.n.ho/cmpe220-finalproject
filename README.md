# CMPE220-FinalProject

## Name
Credit Card Approval Predictor

## Description
The Credit Card Approval Predictor project is an innovative application designed to assess the likelihood of a user being approved for a credit card based on their input data. Utilizing machine learning techniques, the application processes user information entered through a web-based interface and predicts the probability of approval by a credit card company.

## Installation
1. Clone repository:
    - git clone https://gitlab.com/ashley.n.ho/cmpe220-finalproject"
2. Install Flask (Python 3.8 or newer):
    - pip install -m Flask
3. Run website:
    - python -m Website.credit_website
4. Open website at: 
    - http://127.0.0.1:3000/

## Authors
- Faaris Khilji -contributer of the ML_Model_v2 folder
- Jad Abed -contributer of the ML_Model folder
- Akash Kishorbhai Vegada -contributer of the Website folder in collaboration with Ashley Ho
- Ashley Ho -contributer of the Website folder in collaboration with Akash Kishorbhai Vegada

