import numpy as np
import pandas as pd
from keras.models import load_model #using keras.models and not keras.model, version diff?
import pickle
import os

current_dir = os.getcwd() + "/ML_Model_v2"

print("CURR: ", current_dir)

def preprocess_data(data):
    process_data = 0
    scaler_income = 0
    scaler_days_birth = 0
    scaler_days_employed = 0
    
    #load the data scalers
    with open(current_dir + "/scaler_income.pkl", "rb") as file:
        scaler_income = pickle.load(file)
    with open(current_dir + "/scaler_days_birth.pkl", "rb") as file:
        scaler_income = pickle.load(file)
    with open(current_dir + "/scaler_days_employed.pkl", "rb") as file:
        scaler_income = pickle.load(file)
    
    #column names
    column_names = ['CODE_GENDER','FLAG_OWN_CAR', 'FLAG_OWN_REALTY',
                    'CNT_CHILDREN', 'AMT_INCOME_TOTAL', 'NAME_INCOME_TYPE',
                   'NAME_EDUCATION_TYPE', 'NAME_FAMILY_STATUS', 'NAME_HOUSING_TYPE',
                   'DAYS_BIRTH', 'DAYS_EMPLOYED', 'FLAG_MOBIL', 'FLAG_WORK_PHONE',
                   'FLAG_PHONE', 'FLAG_EMAIL', 'OCCUPATION_TYPE', 'CNT_FAM_MEMBERS']
    
    
    #verify input data
    if(len(data) != 17):
        print("Input data error! Expected 16 inputs, got: ", len(data))
        return 1
    else:
        print("Processing data...")
        df = pd.DataFrame(data.reshape(1, -1), columns=column_names)
        
        #return df
        
        #drop unused columns
        df.drop('CODE_GENDER', axis=1, inplace=True)
        
        
        #Map data to integer values
        df['FLAG_OWN_CAR'] = df['FLAG_OWN_CAR'].map({'Y': 1, 'N': 0})
        df['FLAG_OWN_REALTY'] = df['FLAG_OWN_REALTY'].map({'Y': 1, 'N': 0})
        
        mapping_income = {'Working': 1, 'Commercial Associate': 2, 'State servant': 3, 'Student': 4, 'Pensioner': 5}
        mapping_education = {'Secondary / secondary special': 1, 
                   'Higher education': 2, 
                   'Incomplete higher': 3, 
                   'Lower secondary': 4, 
                   'Academic degree': 5}
        mapping_family_status = {'Married': 1, 
                   'Single / not married': 2, 
                   'Civil marriage': 3, 
                   'Separated': 4, 
                   'Widow': 5}
        mapping_housing = {'House / apartment': 1, 
                   'Rented apartment': 2, 
                   'Municipal apartment': 3, 
                   'With parents': 4, 
                   'Co-op apartment': 5, 
                   'Office apartment': 6}
        mapping_occupation = {'Security staff': 1, 
                   'Sales staff': 2, 
                   'Accountants': 3, 
                   'Laborers': 4, 
                   'Managers': 5, 
                   'Drivers': 6, 
                   'Core staff': 7, 
                   'High skill tech staff': 8, 
                   'Cleaning staff': 9, 
                   'Private service staff': 10, 
                   'Cooking staff': 11, 
                   'Low-skill Laborers': 12, 
                   'Medicine staff': 13, 
                   'Secretaries': 14, 
                   'Waiters/barmen staff': 15, 
                   'HR staff': 16, 
                   'Realty agents': 17, 
                   'IT staff': 18}

        df['NAME_INCOME_TYPE']    = df['NAME_INCOME_TYPE'].map(mapping_income)
        df['NAME_EDUCATION_TYPE'] = df['NAME_EDUCATION_TYPE'].map(mapping_education)  
        df['NAME_FAMILY_STATUS']  = df['NAME_FAMILY_STATUS'].map(mapping_family_status)  
        df['NAME_HOUSING_TYPE']   = df['NAME_HOUSING_TYPE'].map(mapping_housing)
        df['OCCUPATION_TYPE']     = df['OCCUPATION_TYPE'].map(mapping_occupation)
        
        #return df
        
        normalized_col = scaler_income.fit_transform(df[['AMT_INCOME_TOTAL']])
        df['AMT_INCOME_TOTAL'] = normalized_col
        
        normalized_col = scaler_income.fit_transform(df[['DAYS_BIRTH']])
        df['DAYS_BIRTH'] = normalized_col
        
        normalized_col = scaler_income.fit_transform(df[['DAYS_EMPLOYED']])
        df['DAYS_EMPLOYED'] = normalized_col
        
        processed_data = np.array(df)
        
    return processed_data


def DT_predict(data):
    print(data)
    data = np.array(data)
    file_path = current_dir + "/decision_tree_F.pkl"
    proc_data = preprocess_data(data)
    print(proc_data)
    with open(file_path, "rb") as file:
        clf = pickle.load(file)
        pred = clf.predict(proc_data)
    return pred

def NN_predict(data):
    print(data)
    data = np.array(data)
    model = load_model(current_dir + "/NN_model_F.h5")
    proc_data = preprocess_data(data)
    print(proc_data)
    pred = model.predict(proc_data.reshape(1,16).astype(np.float32))
    for i in range(0, len(pred)):
        if pred[i] > 0.70:
            pred[i] = 1
        else:
            pred[i] = 0
    return pred.astype(int).flatten()

def RF_predict(data):
    print(data)
    data = np.array(data)
    file_path = current_dir + "/random_forest_model_F.pkl"
    proc_data = preprocess_data(data)
    with open(file_path, "rb") as file:
        clf = pickle.load(file)
        pred = clf.predict(proc_data)
    return pred

if __name__ == "__main__":
    current_dir = os.getcwd()
    print("Running example")
    print("Reading in application_record.csv...")
    df = pd.read_csv('application_record.csv')
    example_data = df.loc[0] # one person data
    example_data = np.array(example_data)
    example_data[-2] = "Drivers" #fill in missing data just for example
    example_data = np.delete(example_data, 0) # get rid of ID field


    pred = DT_predict(example_data)
    pred1 = RF_predict(example_data)
    pred2 = NN_predict(example_data)

    

    print("DT Approved: ", pred)
    print("RF Approved: ", pred1)
    print("NN Approved: ", pred2)