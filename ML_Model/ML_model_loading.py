import numpy
from sklearn.preprocessing import StandardScaler 
import pickle

import warnings
warnings.filterwarnings("ignore") 


def preprocess_data(data):
    processed_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    if data[1] == 'Y':
        processed_data[0] = 1;
    else:
        processed_data[0] = 0;
    if data[2] == 'Y':
        processed_data[1] = 1;
    else:
        processed_data[1] = 0;

    processed_data[2] = data[3]
    processed_data[3] = data[4]
    processed_data[4] = data[9]
    processed_data[5] = data[10]*365
    processed_data[6] = data[16]
    if data[5] == 'Commercial Associate':
         processed_data[7] = 1;
    elif data[5] == 'Pensioner':
         processed_data[8] = 1;
    elif data[5] == 'State Servant':
         processed_data[9] = 1;
    elif data[5] == 'Student':
         processed_data[10] = 1;
    elif data[5] == 'Working':
         processed_data[11] = 1;

    if data[6] == 'Academic degree':
         processed_data[12] = 1;
    elif data[6] == 'Higher education':
         processed_data[13] = 1;
    elif data[6] == 'Incomplete higher':
         processed_data[14] = 1;
    elif data[6] == 'Lower secondary':
         processed_data[15] = 1;
    elif data[6] == 'Secondary / secondary special':
         processed_data[16] = 1;
    
    if data[7] == 'Civil marriage':
         processed_data[17] = 1;
    elif data[7] == 'Married':
         processed_data[18] = 1;
    elif data[7] == 'Separated':
         processed_data[19] = 1;
    elif data[7] == 'Single / not married':
         processed_data[20] = 1;
    elif data[7] == 'Widow':
         processed_data[21] = 1;
    
    if data[8] == 'Co-op apartment':
         processed_data[22] = 1;
    elif data[8] == 'House / apartment':
         processed_data[23] = 1;
    elif data[8] == 'Municipal apartment':
         processed_data[24] = 1;
    elif data[8] == 'Office apartment':
         processed_data[25] = 1;
    elif data[8] == 'Rented apartment':
         processed_data[26] = 1;
    elif data[8] == 'With parents':
         processed_data[27] = 1;
    
   #print (processed_data)
    #processed_data = [0,0,2,157500,12636,5345,4,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0] #sample that will fail
    print (processed_data)
    return [processed_data]


def KKN_predict(data):
    transform = preprocess_data(data)
    KNN_file = "KNN_ML_Model.pkl"
    with open(KNN_file, 'rb') as file:  
        model = pickle.load(file)
        
    scaler_file = "scaler_file.pkl"
    with open(scaler_file, 'rb') as file:  
        scaler = pickle.load(file)
        
    input_data_scaled = scaler.transform(transform)

    results = model.predict(input_data_scaled)

    if results == 1: #swap as results are flipped
        results = 0;
    else:
        results = 1;    
    
    return results

def RFC_2_predict(data):
    transform = preprocess_data(data)
    RFC_file = "RFC_ML_Model.pkl"
    with open(RFC_file, 'rb') as file:  
        model = pickle.load(file)
        
    scaler_file = "scaler_file.pkl"
    with open(scaler_file, 'rb') as file:  
        scaler = pickle.load(file)
        
    input_data_scaled = scaler.transform(transform)

    results = model.predict(input_data_scaled)
 
    if results == 1: #swap as results are flipped
        results = 0;
    else:
        results = 1; 
    
    return results