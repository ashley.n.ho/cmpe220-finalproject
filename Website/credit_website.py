from flask import Flask, render_template, request, jsonify, redirect, url_for
from datetime import datetime
from ML_Model_v2 import CCA_predictor as ML2
from ML_Model import ML_model_loading as ML

# Added the static_url_path argument for images and css - Akash 
app = Flask(__name__,  static_url_path='/static')
app.secret_key = 'supersecretkey'  # Required for session to work

data = [None] * 17
result = "No credit card approval"

def ML_model(data, selection):
    global result 
    if selection == 1:
        prediction = ML2.DT_predict(data)
        print("Using Decision tree")
    elif selection == 2:
        prediction = ML2.NN_predict(data)
        print("Using Neural Network")
    elif selection == 3:
        prediction = ML2.RF_predict(data)
        print("Using random forest")
    elif selection == 4:
        prediction = ML.RFC_2_predict(data)
        print("Using random forest, alternative")
    elif selection == 5:
        prediction = ML.KKN_predict(data)
        print("Using KNN Network")
    if(prediction == 1):
        result = "Credit card approved!"
    return result

def calculate_age_in_days(birth_date_str):
    # Define the format of the input date string
    date_format = "%Y-%m-%d"

    # Convert the input string to a datetime object
    birth_date = datetime.strptime(birth_date_str, date_format)

    # Get the current date
    current_date = datetime.now()

    # Calculate the difference in days
    age_in_days = (current_date - birth_date).days

    return age_in_days

@app.route('/', methods=['GET', 'POST'])
def credit_cards():
    if request.method == 'POST':
        data = [None] * 17
        data[0] = "0"  # unused
        data[1] = str(request.form.get('FLAG_OWN_CAR', ''))
        data[2] = str(request.form.get('FLAG_OWN_REALTY', ''))
        data[3] = int(request.form.get('CNT_CHILDREN', ''))
        data[4] = int(request.form.get('AMT_INCOME_TOTAL', ''))
        data[5] = str(request.form.get('NAME_INCOME_TYPE', ''))
        data[6] = str(request.form.get('NAME_EDUCATION_TYPE', ''))
        data[7] = str(request.form.get('NAME_FAMILY_STATUS', ''))
        data[8] = str(request.form.get('NAME_HOUSING_TYPE', ''))
        data[9] = calculate_age_in_days(str(request.form.get('DAYS_BIRTH', '')))
        data[10] = int(request.form.get('DAYS_EMPLOYED', ''))
        data[11] = int(request.form.get('mobile_phone', ''))
        data[12] = int(request.form.get('work_phone', ''))
        data[13] = int(request.form.get('home_phone', ''))
        data[14] = int(request.form.get('email', ''))
        data[15] = str(request.form.get('OCCUPATION_TYPE', ''))
        data[16] = int(request.form.get('CNT_FAM_MEMBERS', ''))
        
        selection = int(request.form.get('ML_Model_type', 0))
        #call ML functions here
        if(selection):
            prediction = ML_model(data, selection)

        return redirect(url_for('display_data'))
    return render_template('credit_cards.html')

@app.route('/display_data', methods=['GET'])
def display_data():
    # Retrieve the data from session
    
    return render_template('display_data.html', result=result)

if __name__ == '__main__':
    app.run(debug=True, port=3000)

# Added  port=3000 as it was not working with port 5000 for me, You can remove the port argument to revert the changes
# - Akash